package nl.lung.chunfei;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.net.InetAddress;
import java.net.UnknownHostException;

@Path("/whoami")
public class WhoamiResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String whoami() throws UnknownHostException {
        var hostname = InetAddress.getLocalHost().getHostName();
        return "I am " + hostname;
    }
}
